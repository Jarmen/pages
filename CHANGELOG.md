### Future releases  
 * basic pages route in configuration file
 * eloquent model tests
 * integration tests

### Release v1.0.0-beta date: 19.10.2018
Added:
 * "active" field in migration and model mass assignment exception
 * variables and collections hint in blade template

### Release v0.1.4-alpha date: 19.10.2018
Fixed:
 * route model binding

### Release v0.1.3-alpha date: 17.10.2018
Added:
 * pages list implementation for header and footer menus sorted by menu_order field
 * blade templates for header and footer menus

### Release v0.1.2-alpha date: 17.10.2018
Added:
 * CONTRIBUTING.md according to "CONTRIBUTING.md must have"
 * route model binding of Page model via slug field instead of id

### Release v0.0.2-alpha date: 17.10.2018
Added:
 * README.md according to "Package README.md must have"

### Release v0.0.1-alpha date: 16.10.2018
Added:
 * basic package structure according to https://bitbucket.org/lenalltd/lenal_standards/wiki/automation_php
 * empty README.md 
 * CHANGELOG.md according to "Package CHANGELOG.md must have"
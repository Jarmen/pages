## lenal/pages  
  
Simple pages like "Contacts", "About" etc  
  
### Install  
  
  * Add to composer.json  
  
  ```  
  "repositories": [  
          {  
              "type": "git",  
              "url":  "git@gitlab.com:Jarmen/pages.git"  
          }  
      ]  
  ```   
  
  If prompted, insert login/password or create auth.json in your global composer directory
    
  * Run command  
  
  ```
  composer require lenal/pages  
  ```  
  
  ### Basic usage  
  * Run command  
  
  ```
  php artisan vendor:publish --provider=lenal\pages\PagesServiceProvider --tag=config
  ```  
  
  to copy configuration file into project
      
  * Run command 
  
  ```
    php artisan migrate
   ```  
    
  ### Package customization  
  
  * Run command  
  for laravel <= 5.4 
  
  ```
    php artisan vendor:publish --provider=lenal\pages\PagesServiceProvider
  ```  
  
   for laravel >= 5.5 
    
    ```
      php artisan vendor:publish 
    ```  
   
   select package from list
  
  All files wiil be copied to packages/lenal/pages   
  
  * Register package namespace in composer.json  
  
  ```
    #!json
    "autoload": {
            "psr-4": {
                "lenal\\pages\\": "packages/lenal/pages/src"
            }
        },
   ```   
   
   * Add service provider to 'providers' section in  config/app.php  
   
   ```  
   /*  
    * Package Service Providers...  
    */  
    lenal\pages\PagesServiceProvider::class,  
   ```  
   
   * Run command  
   
   ```
   composer dumpautoload
   ```
   
   To prevent namespace collision remove package from vendor
   
   ```
   composer remove lenal/pages
   ```  
   
   
   ### Use cases and mockups
    https://gitlab.com/Jarmen/pages/wikis/home
  
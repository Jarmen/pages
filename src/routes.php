<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/page/{page}', 'PagesController@show');
});
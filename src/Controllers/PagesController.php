<?php

namespace lenal\pages\Controllers;

use App\Http\Controllers\Controller;
use lenal\pages\Models\Page;

class PagesController extends Controller
{

    public function show(Page $page)
    {
        return view('pages::abstract_simple_page', compact('page'));
    }
}
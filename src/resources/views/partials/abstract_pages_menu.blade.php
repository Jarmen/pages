{{--
$pages - all active pages collection
$page->slug - uri
$page->name - title
--}}
<ul>
    @foreach ($pages as $page)
        <li>
            <a href="{{ route('/page/' . $page->slug) }}">
                {{ $page->name }}
            </a>
        </li>
    @endforeach
</ul>
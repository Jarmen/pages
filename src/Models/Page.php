<?php

namespace lenal\pages\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'slug', 'meta_title', 'meta_description',
        'meta_keywords', 'content', 'show_in_menu', 'menu_order',
        'active'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopePagesMenu($query)
    {
        return $query->where('show_in_menu', '>', 0)
            ->where('active', '>', 0)
            ->orderBy('menu_order');
    }
}

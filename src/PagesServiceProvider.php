<?php

namespace lenal\pages;

use Illuminate\Support\ServiceProvider;

class PagesServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('pages', 'lenal\pages\Helpers\Pages');
    }

    public function boot()
    {
        $this->loadViewsFrom(realpath(dirname(__FILE__)) . '/resources/views', 'pages');
        $this->loadRoutesFrom(realpath(dirname(__FILE__)) . '/routes.php');
        $this->loadMigrationsFrom(realpath(dirname(__FILE__)) . '/migrations');

        $this->publishes([
            realpath(dirname(__FILE__) . '/config/pages.php') => config_path('pages.php')
        ], 'config');

        $this->publishes([
            base_path('vendor/lenal/pages/') => base_path('packages/lenal/pages')
        ]);
    }
}